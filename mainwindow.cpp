#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_pPresenter = new MainPresenter(this);

    connect(ui->pushButtonProgram, SIGNAL(released()), this, SLOT(onStartProgrammingClick()));
}

MainWindow::~MainWindow()
{
    delete ui;
    if (m_pPresenter != NULL)
    {
        delete m_pPresenter;
        m_pPresenter = NULL;
    }
}

void MainWindow::appendToListing(const QString &text)
{
    //ui->textEditListing->insertPlainText(text);
    ui->textEditListing->append(text);
}

void MainWindow::bddErreur(const QString &message)
{
    QMessageBox::critical(this, "Erreur", message);
}

void MainWindow::onStartProgrammingClick()
{
    m_pPresenter->onStartProgramming();
}
