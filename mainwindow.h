#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "mainpresenter.h"

class MainPresenter;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow *ui;
    MainPresenter *m_pPresenter;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public:
    void appendToListing(const QString &text);

    void bddErreur(const QString &message);

private slots:
    void onStartProgrammingClick();
};

#endif // MAINWINDOW_H
