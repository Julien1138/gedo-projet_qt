#include "mainpresenter.h"

MainPresenter::MainPresenter(MainWindow *View)
    : m_pView(View)
{
    m_pInterfaceGedo = new InterfaceGedo(this);
}

MainPresenter::~MainPresenter()
{
    if (m_pInterfaceGedo != NULL)
    {
        delete m_pInterfaceGedo;
        m_pInterfaceGedo = NULL;
    }
}

void MainPresenter::onStartProgramming()
{
    notifyPresenter("Connexion à Gédo...");
    m_pInterfaceGedo->connectionToGedo();
}

void MainPresenter::onGedoError()
{
    notifyPresenter("ERREUR : Gédo renvoie une erreur");
    m_pView->bddErreur("Gédo renvoie une erreur.");
}

void MainPresenter::onGedoConnection(bool result)
{
    if (result)
    {
        notifyPresenter("Connecté à Gédo");
        notifyPresenter("\nPréparation de la mémoire...");
        m_pInterfaceGedo->clearZones();
    }
    else
    {
        notifyPresenter("ERREUR : Gédo non conecté");
        m_pView->bddErreur("Impossible de se connecter à Gédo.\n\nEssayez de débrancher et rebranger Gédo au PC.");
    }
}

void MainPresenter::onMemoryReady(bool result)
{
    if (result)
    {
        notifyPresenter("Mémoire prête");
        notifyPresenter("\nProgrammation des zones...");
        m_pInterfaceGedo->configZone(-1, 156384871974, 12, -156877789433465, 798, 6646, -4564, 0);
    }
    else
    {
        notifyPresenter("ERREUR : Problème de connexion avec Gédo");
        m_pView->bddErreur("Problème de connexion avec Gédo.\n\nVeuillez réessayer.");
    }
}

void MainPresenter::notifyPresenter(const QString &text)
{
    m_pView->appendToListing("<html><b>" + text + "</b></html>");
}

void MainPresenter::notifyInterfaceGedo(const QString &text)
{
    m_pView->appendToListing("    " + text);
}

void MainPresenter::notifySerialWrite(const QString &text)
{
    m_pView->appendToListing("<html><i>" + text + "</i></html>");
}

void MainPresenter::notifySerialRead(const QString &text)
{
    m_pView->appendToListing("<html><i>&nbsp;&nbsp;&nbsp;&nbsp;" + text + "</i></html>");
}
