#ifndef MAINPRESENTER_H
#define MAINPRESENTER_H

#include "mainwindow.h"
#include "interfacegedo.h"

class MainWindow;
class InterfaceGedo;

class MainPresenter
{

private:
    MainWindow    *m_pView;
    InterfaceGedo *m_pInterfaceGedo;

public:
    MainPresenter(MainWindow *View);
    ~MainPresenter();

public:
    void onStartProgramming();

    void onGedoError();
    void onGedoConnection(bool result);
    void onMemoryReady(bool result);

    void notifyInterfaceGedo(const QString &text);
    void notifySerialWrite(const QString &text);
    void notifySerialRead(const QString &text);

private:
    void notifyPresenter(const QString &text);
};

#endif // MAINPRESENTER_H
