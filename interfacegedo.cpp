#include "interfacegedo.h"
#include <QApplication>

#define CONNECTION_TIMEOUT_MS   2000
#define CONNECTION_TENTATIVES   1
#define CONFIG_CMD  "$CONFIG*0A"
#define CLRZN_CMD "$CLRZN*49"
#define CFGZN_CMD "$CFGZN"

InterfaceGedo::InterfaceGedo(MainPresenter *Presenter)
    :m_pPresenter(Presenter)
    ,m_iTentativesRestantes(0)
    ,m_SerialBuffer("")
    ,m_ActionEnCours("")
{
    m_pSerial = new QSerialPort();
    QObject::connect(m_pSerial, SIGNAL(readyRead()), this, SLOT(onSerialReceived()));

    m_pTimer = new QTimer();
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(onTimerTimeout()));

    m_ComPortList.clear();
}

InterfaceGedo::~InterfaceGedo()
{
    if (m_pSerial->isOpen())
    {
        m_pSerial->close();
    }
    if (m_pSerial != NULL)
    {
        delete m_pSerial;
        m_pSerial = NULL;
    }
}

void InterfaceGedo::connectionToGedo()
{
    m_ComPortList.clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {

        if (info.description().startsWith("Arduino", Qt::CaseInsensitive))
            m_ComPortList.append(info.portName());
    }
    m_iTentativesRestantes = CONNECTION_TENTATIVES;
    this->connectionTryOnNextComPort();
}

void InterfaceGedo::clearZones()
{
    if (m_pSerial->isOpen())
    {
        m_ActionEnCours = "CLRZN";
        m_pPresenter->notifySerialWrite(CLRZN_CMD);
        m_pSerial->write(CLRZN_CMD);
        m_pSerial->write("\n");
    }
}

void InterfaceGedo::configZone(long lLatitude_1, long lLongitude_1,
                               long lLatitude_2, long lLongitude_2,
                               long lLatitude_3, long lLongitude_3,
                               long lLatitude_4, long lLongitude_4)
{
    if (m_pSerial->isOpen())
    {
        m_ActionEnCours = "CFGZN";
        QString message = CFGZN_CMD;
        message.append(",");
        message.append(QString::number(lLatitude_1));
        message.append(",");
        message.append(QString::number(lLongitude_1));
        message.append(",");
        message.append(QString::number(lLatitude_2));
        message.append(",");
        message.append(QString::number(lLongitude_2));
        message.append(",");
        message.append(QString::number(lLatitude_3));
        message.append(",");
        message.append(QString::number(lLongitude_3));
        message.append(",");
        message.append(QString::number(lLatitude_4));
        message.append(",");
        message.append(QString::number(lLongitude_4));

        appendChecksum(message);

        m_pPresenter->notifySerialWrite(message);
        m_pSerial->write(message.toStdString().c_str());
        m_pSerial->write("\n");
    }
}


void InterfaceGedo::connectionTryOnNextComPort()
{
    if (m_ComPortList.isEmpty())
    {
        if (m_pSerial->isOpen())
        {
            m_pSerial->close();
        }
        m_pPresenter->onGedoConnection(false);
        return;
    }

    QString portName = m_ComPortList.last();
    m_iTentativesRestantes--;
    if (m_iTentativesRestantes == 0)
    {
        m_ComPortList.removeLast();
        m_iTentativesRestantes = CONNECTION_TENTATIVES;
    }

    m_pPresenter->notifyInterfaceGedo("Recherche sur le port " + portName + "...");
    m_pSerial->setPortName(portName);

    m_pSerial->open(QSerialPort::ReadWrite);

    m_pSerial->setBaudRate(QSerialPort::Baud9600);
    m_pSerial->setDataBits(QSerialPort::Data8);
    m_pSerial->setFlowControl(QSerialPort::NoFlowControl);
    m_pSerial->setParity(QSerialPort::NoParity);
    m_pSerial->setStopBits(QSerialPort::OneStop);
    m_pSerial->setDataTerminalReady(true);

    m_ActionEnCours = "CONFIG";

    m_pPresenter->notifySerialWrite(CONFIG_CMD);
    m_pSerial->write(CONFIG_CMD);
    m_pSerial->write("\n");

    m_pTimer->setSingleShot(true);
    m_pTimer->start(CONNECTION_TIMEOUT_MS);
}

void InterfaceGedo::extractGedoMessages()
{
    while (!m_SerialBuffer.isEmpty())
    {
        int messageSize = m_SerialBuffer.indexOf("\n");

        if (messageSize == -1)
            return;

        QString message = m_SerialBuffer.left(messageSize + 1);
        m_SerialBuffer.remove(0, messageSize + 1);
        m_pPresenter->notifySerialRead(message);

        if (this->messageIsValid(message))
        {
            this->decodeGedoMessages(message);
        }
        else
        {
            // TODO : avertir le presenter
        }
    }
}

bool InterfaceGedo::messageIsValid(const QString &message)
{
    char checksum = 0;
    int indexOfChecksum = message.indexOf('*');
    for (int i = 1 ; i < indexOfChecksum ; i++)
    {
        checksum ^= (char) message.at(i).toLatin1();
    }
    QString strChecksum = QString::number((unsigned int)checksum, 16);
    if (strChecksum.length() == 1)
    {
        strChecksum = '0' + strChecksum;
    }

    if (message.mid(indexOfChecksum+1, 2).compare(strChecksum, Qt::CaseInsensitive) == 0)
        return true;

    return false;
}

void InterfaceGedo::decodeGedoMessages(const QString &message)
{
    if (message.startsWith("$ERR"))
    {
        m_pTimer->stop();
        m_ActionEnCours = "";
        m_pPresenter->onGedoError();
    }
    else if (message.startsWith("$ACK"))
    {

        if (message.startsWith("$ACK,CONFIG*6F") && m_ActionEnCours.compare("CONFIG") == 0)
        {
            m_pTimer->stop();
            m_ActionEnCours = "";
            m_pPresenter->onGedoConnection(true);
        }
        if (message.startsWith("$ACK,CLRZN*2C") && m_ActionEnCours.compare("CLRZN") == 0)
        {
            m_pTimer->stop();
            m_ActionEnCours = "";
            m_pPresenter->onMemoryReady(true);
        }
    }
}

void InterfaceGedo::appendChecksum(QString &message)
{
    message.append('*');
    char checksum = 0;
    for (int i = 1 ; i < message.indexOf('*') ; i++)
    {
        checksum ^= (char) message.at(i).toLatin1();
    }
    message.append(QString::number((unsigned int)checksum, 16));
}



void InterfaceGedo::onSerialReceived()
{
    QByteArray buffer = m_pSerial->readAll();
    m_SerialBuffer += QString::fromStdString(buffer.toStdString());
    this->extractGedoMessages();
}

void InterfaceGedo::onTimerTimeout()
{
    if (m_pSerial->isOpen())
    {
        m_pSerial->close();
    }
    if (m_ActionEnCours.compare("CONFIG") == 0)
    {
        m_ActionEnCours = "";
        this->connectionTryOnNextComPort();
    }
    if (m_ActionEnCours.compare("CLRZN") == 0)
    {
        m_ActionEnCours = "";
        m_pPresenter->onMemoryReady(false);
    }
}
