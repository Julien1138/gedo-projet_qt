#ifndef INTERFACEGEDO_H
#define INTERFACEGEDO_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QTimer>

#include "mainpresenter.h"

class MainPresenter;

class InterfaceGedo : public QObject
{
    Q_OBJECT

private:
    MainPresenter   *m_pPresenter;
    QSerialPort     *m_pSerial;
    QTimer          *m_pTimer;

    QStringList     m_ComPortList;
    int             m_iTentativesRestantes;
    QString         m_SerialBuffer;
    QString         m_ActionEnCours;

public:
    InterfaceGedo(MainPresenter *Presenter);
    ~InterfaceGedo();
    void connectionToGedo();
    void clearZones();
    void configZone(long lLatitude_1, long lLongitude_1,
                    long lLatitude_2, long lLongitude_2,
                    long lLatitude_3, long lLongitude_3,
                    long lLatitude_4, long lLongitude_4);

private:
    void connectionTryOnNextComPort();
    void extractGedoMessages();
    bool messageIsValid(const QString &message);
    void decodeGedoMessages(const QString &message);
    void appendChecksum(QString &message);

private slots:
    void onSerialReceived();
    void onTimerTimeout();
};

#endif // INTERFACEGEDO_H
