#-------------------------------------------------
#
# Project created by QtCreator 2016-07-11T17:24:04
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Gedo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mainpresenter.cpp \
    interfacegedo.cpp

HEADERS  += mainwindow.h \
    mainpresenter.h \
    interfacegedo.h

FORMS    += mainwindow.ui
